<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;



class ItemController extends Controller

{
    // GET method to retrieve all items
    public function index()
    {
        return response()->json(Item::all(), 200);
    }

    // POST method to create a new item
    public function store(Request $request)
    {
        $item = Item::create($request->all());
        return response()->json($item, 201);
    }

    // PUT method to update an existing item
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $item->update($request->all());
        return response()->json($item, 200);
    }
}
