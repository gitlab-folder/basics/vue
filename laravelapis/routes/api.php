<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ItemController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/laravel_api', function () {
    Log::info('API endpoint flag' ,[
        'user_id' => 1
    ]);
    return ('Hello Api');
});

// GET route to retrieve all items
Route::get('/items', [ItemController::class, 'index']);

// POST route to create a new item
Route::post('/items', [ItemController::class, 'store']);

// PUT route to update an existing item
Route::put('/items/{item}', [ItemController::class, 'update']);
