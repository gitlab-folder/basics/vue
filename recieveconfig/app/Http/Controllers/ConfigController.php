<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Config;


class ConfigController extends Controller
{
    public function configsean()
    {
        $sean = config('sean.Sean.message');

        Log::info($sean);
    }
    public function configIf()
    {
        $index = 1;

        $returnelse = config('configelse.elseReturn.message');
        $returnif = config('configIf.ifReturn.message');


        if ($index == 0) {
            Log::info($returnif);
        } else {
            Log::info($returnelse);
        }
    }

    public function configWhile()
    {
        $index = 0;

        $returnwhile = config('configWhile.whilereturn.message');


        Log::info('before the loop');
        while ($index == 0) {
            Log::info($returnwhile);
            break;
        }
        Log::info('outside/end of loop');
    }
}
