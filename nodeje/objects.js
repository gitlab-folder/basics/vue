var ourDog = {              //Object class
    "name": "lexi",
    "legs": 4,              // Property:value
    "tails": 1,
    "friends":"everything!",
    "favourite food": "pedigree Beef"
};

var dogName = ourDog.name; //dot notation to find a value from a property
var dogFood = ourDog["favourite food"]

console.log(dogName,dogFood);