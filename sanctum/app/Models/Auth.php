<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Auth extends Model
{
    use HasFactory;
    public function login(){

        return view('auth.login');
    }

    public function authenticate(){



        $validated = request()->validate(

            [

                'email' => 'required|email',
                'password' => 'required|min:6'
            ]
            );

            if (auth()->attempt($validated)) {
                request()->session()->regenerate();

                return redirect()->route('home') ->with('success', 'Account created succesfully!');
            }

            return redirect()->route('login')->withErrors([
                'email' => "Invalid Email or password."
            ]);

    }
}
