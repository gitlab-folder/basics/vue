<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
       $User1=User::create([
        'name'=>'Sean',
        'email'=>'Sean@email.com',
        'password'=>Hash::make('password')

       ]);
       $User3=User::create([
        'name'=>'Adam',
        'email'=>'Adam@email.com',
        'password'=>Hash::make('password')

       ]);
       $User2=User::create([
        'name'=>'Jamie',
        'email'=>'Jamie@email.com',
        'password'=>Hash::make('password')

       ]);


    }
}
